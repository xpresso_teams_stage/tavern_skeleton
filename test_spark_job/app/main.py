"""
An example of how to use DataFrame for ML
"""
__author__ = "Naveen Sinha"


import os
import sys
import argparse
from pyspark.sql import SparkSession
import xpresso.ai.core.commons.utils.constants as constants
from app.custom_component import CustomComponentTransformer, \
    CustomComponentEstimator
from xpresso.ai.core.data.distributed.automl.distributed_structured_dataset \
    import DistributedStructuredDataset

if __name__ == "__main__":
    print('Running...', flush=True)

    parser = argparse.ArgumentParser()

    # Parse the arguments as give at the time of component deployment
    # extra/missing args will result in error and termination of this
    # program

    parser.add_argument('--arg-name-1', type=str, default='arg-value-1',
                        help='args 1 help')
    parser.add_argument('--arg-name-2', type=str, default='arg-value-2',
                        help='arg 2 help')
    parser.add_argument('--run_name', type=str, default='run_name-value',
                        help='run_name help')
    args = parser.parse_args()
    run_id = None
    if args:
        run_id = args.run_name
    print(f'All: {args}', flush=True)
    print(run_id, flush=True)

    spark = SparkSession \
        .builder \
        .getOrCreate()

    dataset = DistributedStructuredDataset()
    config = {
        "type": "FS",
        "dataset_type": "distributed",
        "path": "/tavern-data/test.csv",
        "options": {"sep": ","}
    }
    dataset.import_dataset(config)
    dataset.data.to_csv(
        constants.hdfs_URI.format(
            "172.16.1.81", "8020", "/tavern-data/test_spark_job/output.csv")
    )

    spark.stop()
