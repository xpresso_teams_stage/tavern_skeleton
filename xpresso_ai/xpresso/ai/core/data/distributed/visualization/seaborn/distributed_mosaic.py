"""
    Mosaic plot for distributed data
"""
__all__ = ['distributed_mosaic']
__author__ = 'Sanyog Vyawahare'

import matplotlib.pyplot as plt
import databricks.koalas as ks
import seaborn as sns
from statsmodels.graphics.mosaicplot import mosaic
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InputLengthMismatch, AttributeNotPresent


class NewPlot(Plot):
    """
    Generates a mosaic plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_data(DataFrame): data for plot
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
    """
    def __init__(self, input_data, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH, columns=None):
        super().__init__()
        sns.set(font_scale=1.5)
        if axes_labels:
            self.axes_labels = axes_labels
        if isinstance(input_data, ks.DataFrame):
            input_data = input_data.dropna()
            try:
                input_1 = input_data[columns[0]]
                input_2 = input_data[columns[1]]
            except KeyError:
                raise AttributeNotPresent
        else:
            raise AttributeNotPresent
        if len(input_1) != len(input_2):
            raise InputLengthMismatch
        self.figure, self.plot = plt.subplots(figsize=self.figure_size)
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL])
        data = input_data.groupby([columns[0], columns[1]]).size().reset_index()

        pdf = data.to_pandas()
        pdf['combine'] = list(zip(pdf[columns[0]], pdf[columns[1]]))
        plot_data = dict(zip(pdf['combine'], pdf['count']))
        mosaic(plot_data, labelizer=lambda x: "", ax=self.plot, title=plot_title)

        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name)
