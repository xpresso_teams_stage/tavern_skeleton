"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "Naveen Sinha"

import time
import os
import pandas as pd

if __name__ == '__main__':
    mount_path = "/data"
    file_name = os.path.join(mount_path, "test.csv")
    print(f"listdir in {mount_path}: {os.listdir(mount_path)}")
    print(f"Reading CSV file: {file_name}")
    time.sleep(120)
    data = pd.read_csv(file_name)
    output_file_name = os.path.join(mount_path, "output_data.csv")
    data.to_csv(output_file_name, index=False)
    print(f"Output CSV stored in: {output_file_name}")
    print(f"listdir in {mount_path}: {os.listdir(mount_path)}")
    time.sleep(30)
